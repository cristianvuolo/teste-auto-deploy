<?php
use PHPUnit\Framework\TestCase;

class defaultTest extends TestCase
{
    // ...

    public function testCanBeNegated()
    {
        $this->assertEquals(-1, 1);
    }

    // ...
}
