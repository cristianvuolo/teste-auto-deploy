#!/usr/bin/env bash
echo "### Baixando Nova verção do repositório ###"
git pull
echo "### Rodando Compose Update ###"
composer update
echo "### Executando Migrações ###"
php artisan migrate
echo "### Script Executado ###"